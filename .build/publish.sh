#!/usr/bin/env bash

    # Get root level directory information
    # () force it to be an array
    charts=(`ls *.tgz -l | awk '{print $9}'`)

    # Loop through each array item
    for(( i=0; i<${#charts[@]}; i++))
    do
      curl -L --data-binary "@${charts[$i]}" http://charts.ci/api/charts  -o /dev/null -w '%{http_code}\n' -s
      echo "${charts[$i]}"
    done